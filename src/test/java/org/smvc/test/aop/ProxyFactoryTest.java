package org.smvc.test.aop;

import org.junit.Test;
import org.smvc.framework.aop.ProxyFactory;
import org.smvc.framework.aop.interceptor.Interceptor;

import net.sf.cglib.core.DebuggingClassWriter;

public class ProxyFactoryTest
{

	@Test
	public void doTest() throws Exception
	{
		System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "gen.class");
		// haveAuth();
		haveNoAuth();
	}

	public void doMethod(TableDao dao)
	{
		dao.create();
		dao.query();
		// dao.update();
		// dao.delete();
	}

	// 模拟有权限
	public void haveAuth()
	{
		TableDao tDao = ProxyFactory.getInstance(TableDao.class,
				new Interceptor[] { new AuthInterceptor("张三"), new LogInterceptor() });
		doMethod(tDao);
		tDao.getClass().getResourceAsStream("TableDao.class");
	}

	// 模拟无权限
	public void haveNoAuth() throws Exception
	{
		TableDao tDao = ProxyFactory.getInstance(TableDao.class,
				new Interceptor[] { new AuthInterceptor("李四"), new LogInterceptor() });
		doMethod(tDao);
		System.out.println(tDao.getClass().getName());
//
//		ClassReader cr = new ClassReader(tDao.getClass().getName());
//		ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
//		cr.accept(new ClassVisitor(Opcodes.ACC_PUBLIC)
//		{
//			public void visit(final int version, final int access, final String name, final String signature,
//					final String superName, final String[] interfaces)
//			{
//				cv.visit(version, access, name, signature, superName, interfaces);
//			}
//
//			@Override
//			public MethodVisitor visitMethod(final int access, final String name, final String desc,
//					final String signature, final String[] exceptions)
//			{
//				return cv.visitMethod(access, name, desc, signature, exceptions);
//			}
//		}, 0);
//		byte[] bytes = cw.toByteArray();

	}
}